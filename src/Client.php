<?php
namespace CodeJunkii\VivaPayments;

/**
 * @author Yiorgos Tavantzopoulos <g.tavantzopoulos@gmail.com>
 * @license LGPLv3.0+
 * @version 1.0
 */
use \GuzzleHttp\Exception\RequestException;

/**
 * This is an unofficial but yet working client for a popular
 * payment gateway in Greece called VivaPayments (http://www.vivapayments.com).
 *
 * @link(https://github.com/VivaPayments/API/wiki, Developer API)
 */
class Client {

    const ENV_TEST              = 'TEST';
    const ENV_SANDBOX           = 'SANDBOX';
    const ENV_PRODUCTION        = 'PRODUCTION';

    const ENDPOINT_TEST         = 'http://127.0.0.1:8125/viva-server';
    const ENDPOINT_SANDBOX      = 'http://demo.vivapayments.com';
    const ENDPOINT_PRODUCTION   = 'https://www.vivapayments.com';

    const _TRUE_          = 'true';
    const _FALSE_         = 'false';

    const TXN_STATUS_ERROR               = 'E'; // The transaction was not completed because of an error
    const TXN_STATUS_UNDERWAY            = 'A'; // The transaction is in progress
    const TXN_STATUS_DISPUTED            = 'M'; // The cardholder has disputed the transaction with the issuing Bank
    const TXN_STATUS_DISPUTED_AWAITING   = 'MA';// Dispute Awaiting Response
    const TXN_STATUS_DISPUTED_INPROGRESS = 'MI';// Dispute Inprogress
    const TXN_STATUS_DISPUTED_REFUNDED   = 'ML';// A disputed transaction has been refunded (Dispute Lost)
    const TXN_STATUS_DISPUTED_REVERSED   = 'MW';// Dispute Won
    const TXN_STATUS_DISPUTED_SUSPECTED  = 'MS';// Suspected Dispute
    const TXN_STATUS_CANCELLED           = 'X'; // The transaction was cancelled by the merchant
    const TXN_STATUS_REFUNDED            = 'R'; // The transaction has been fully or partially refunded
    const TXN_STATUS_COMPLETED           = 'F'; // The transaction has been completed successfully

    const TXN_TYPE_CAPTURE             = 0;  // A Capture event of a preAuthorized Transactions
    const TXN_TYPE_PREAUTH             = 1;  // Authorization hold
    const TXN_TYPE_UPDATETAXCARD       = 3;  // Tax Card receipt transaction
    const TXN_TYPE_REFUNDCARD          = 4;  // Refund transaction
    const TXN_TYPE_CHARGECARD          = 5;  // Card payment transaction*
    const TXN_TYPE_INSTALLMENTS        = 6;  // A card payment that will be done with installments
    const TXN_TYPE_VOID                = 7;  // A cancelled transaction
    const TXN_TYPE_CLAIM_REFUND        = 13; // Claim Refund            Refund transaction for a claimed transaction
    const TXN_TYPE_DIAS                = 15; // Payment made through the DIAS system
    const TXN_TYPE_PAYMENTFROMRESELLER = 16; // Cash Payments, through the Viva Payments Authorized Resellers Network
    const TXN_TYPE_REFUNDINSTALLMENTS  = 18; // A Refunded installment
    const TXN_TYPE_CLEARANCE           = 19; // Clearance of a transactions batch
    const TXN_TYPE_REVERSETAXCARD      = 22; // Refund previous tax card transaction
    const TXN_TYPE_BANKTRANFER         = 24; // Bank Transfer command from the merchant's wallet to their IBAN

    protected $_environment = self::ENDPOINT_SANDBOX;
    protected $_merchantID;
    protected $_apiKey;

    /**
     * @var \GuzzleHttp\Client
     */
    private $_restClient = null;

    /**
     * The username that initiated this action, used optionally for your own logging purposes.
     * It will be set whenever the API supports it.
     * @var string
     */
    public $actionUser      = null;
    /**
     * Flag that show or hides the field for the tax card id
     * @var boolean
     */
    public $allowTaxCard    = false;
    /**
     * How many days an authorization should last.
     * Note that this might not get followed by the bank
     * Always check the transaction info by calling the getTransactionInfo() method
     *
     * @var integer
     */
    public $authorizationDuration = 15; // In Day
    /**
     * The language variant that should used when the user will redirected to VivaPayments
     * page.
     *
     * @var string A language code. Possible values are 'el-GR' and 'en-US'
     */
    public $requestLanguage = 'el-GR';
    /**
     * If the transactions should enabled for recurring transactions
     * @var string
     */
    public $recurring       = false;
    /**
     * The last error. After the parsing a viva response
     * @var string
     */
    public $error           = null;
    /**
     * By default, the life span of every Payment Order is 300 seconds (5 minutes).
     * This is the time given to the cardholder to complete an online payment.
     * If the cardholder does not complete the payment within the given time frame,
     * the Payment Order is automatically cancelled. By using this parameter,
     * you can define a different life span for your Payment Order.
     * Note  Use value 65535 if you want your Payment Order to never expire.
     *
     * @var integer
     */
    public $paymentTimeOut      = 300;

    /**
     * Sources can be created and managed from the merchant's profile.
     * A source is used for grouping and filtering transactions
     * (ie a merchant may have more than one websites, or different product categories).
     *
     * @var string
     */
    public $sourceCode          = null;

    /**
     * Constructs a new instance of the client
     *
     * @param string $env        The working environment
     * @param string $merchantID A merchant id
     * @param string $apiKey     A valid api key for the merchant
     */
    public function __construct($env, $merchantID, $apiKey)
    {
        $this->_environment = $env;
        $this->_merchantID  = $merchantID;
        $this->_apiKey      = $apiKey;
    }

    /**
     * Creates and order that authorize the given amount without
     * capturing it.
     * Note: How then the amount can be captured?
     *
     * @link https://github.com/VivaPayments/API/wiki/CreateOrder
     * @param float $amount The amount of the order
     * @param array $params Additional optional parameters
     * @return mixed        Returns an array with the orderCode and a next url that the user should get redirected to
     *                      or null on error.
     */
    public function authorize($amount, $params=array())
    {
        $expDate = new \DateTime("+{$this->authorizationDuration} days");
        $params = array_merge($params, array(
            'IsPreAuth'=>self::_TRUE_,
            'ExpirationDate'=>$expDate->format(\DateTime::ISO8601)
        ));

        return $this->createOrder($amount, $params);
    }

    /**
     * Creates an order and returns the nextUrl that the user
     * has to be redirected
     *
     * @link https://github.com/VivaPayments/API/wiki/CreateOrder
     * @param float $amount The amount of the order
     * @param array $params Additional optional parameters
     * @return mixed        Returns an array with the orderCode and a next url that the user should get redirected to
     *                      or null on error.
     */
    public function createOrder($amount, $params=array())
    {
        $data = array_merge(array(
            'Amount'=>$this->centify($amount),
            'AllowRecurring'=>$this->recurring?self::_TRUE_:self::_FALSE_,
            'AllowTaxCard'=>$this->allowTaxCard?self::_TRUE_:self::_FALSE_,
            'RequestLang'=>$this->requestLanguage,
            'ActionUser'=>$this->actionUser,
            'PaymentTimeOut'=>$this->paymentTimeOut,
            'SourceCode'=>$this->sourceCode,
        ), $params);

        $url = $this->createUrl('/api/orders');
        $response = null;
        try {
            $response = $this->getRestClient()
                ->post($url, array(
                    'body'=>$data
                ));
        } catch (RequestException $e) {
            $this->error = $e->getMessage();
            return null;
        }

        if (($apiResponse=$this->parseResponse($response)) !== null) {
            $orderId = $apiResponse['OrderCode'];
            $nextUrl = $this->createUrl('/web/newtransaction.aspx', array('ref'=>$orderId));
            return array(
                'orderId'=>$orderId,
                'nextUrl'=>$nextUrl,
                'rawResponse'=>$apiResponse
            );
        }

        return null;
    }

    /**
     * This API call allows you to make a new payment by either committing an already
     * authorized transaction or by making a recurring payment.
     * The latter is only permitted if the following two conditions are met:
     *    - The cardholder has already been charged successfully in the past
     *    - The cardholder has agreed on allowing recurring payments on their card
     *
     * @param  string $origTxnId    The txn id of the original payment.
     * @param  float  $amount       The amount to be charged. The amount requested in cents (amount in euros x 100).
     *                              For capturing authorized transactions, it should not exceed the amount entered
     *                              in the original PreAuth.
     * @param  integer $installments This Optional parameter defines the number of installments for the payment.
     *                              Note: Acceptable values 1 to 48
     * @return array                The viva response or null on error
     */
    public function createRecurringTransaction($origTxnId, $amount, $installments=null)
    {
        $data = array(
            'Amount'=>$this->centify($amount),
        );

        if ($installments !== null && ($installments>=1 and $installments<=48) ) {
            $data['Installments'] = $installments;
        }

        $url = $this->createUrl("/api/transactions/{$origTxnId}");
        $response = null;
        try {
            $response = $this->getRestClient()
                ->post($url, $data);
        } catch (RequestException $e) {
            $this->error = $e->getMessage();
            return null;
        }

        return $this->parseResponse($response);
    }

    /**
     * Cancelling a standing Payment Order is is pretty straightforward.
     * Note that a customer with a valid Payment Order Code can still make a payment
     * using the DIAS channels (e-banking, ATM, Bank branch)
     * even if the order has been cancelled.
     *
     * @link https://github.com/VivaPayments/API/wiki/CancelOrder
     * @param  int $orderId A valid 12 digit Payment Order code. The order Id as taken from the response of the createOrder method
     * @return boolean
     */
    public function cancelOrder($orderId)
    {
        $url = $this->createUrl("/api/orders/{$orderId}");
        $response = null;
        try {
            $response = $this->getRestClient()
                ->delete($url);
        } catch (RequestException $e) {
            $this->error = $e->getMessage();
            return null;
        }

        return $this->parseResponse($response);
    }

    /**
     * Retrieves the transaction info
     *
     * @link(https://github.com/VivaPayments/API/wiki/GetTransactions Get Transactions)
     * @param  string $txnId a transcationId
     * @return array        The transaction info or null on error
     */
    public function getTransactionInfo($txnId)
    {
        $res = $this->getTransactions('txnid', $txnId);
        if ($res !== null) {
            return current($res['Transactions']);
        }

        return null;
    }

    /**
     * Retrieves the transaction for the given orderId
     *
     * @link(https://github.com/VivaPayments/API/wiki/GetTransactions Get Transactions)
     * @param  string $orderCode An 12-digits order code
     * @return array        The transaction info or null on error
     */
    public function getTransactionInfoByOrderCode($orderCode)
    {
        $res = $this->getTransactions('ordercode', $orderCode);
        if ($res !== null) {
            return current($res['Transactions']);
        }

        return null;
    }

    /**
     *  A given day for which all transactions will be returned
     *
     * @link(https://github.com/VivaPayments/API/wiki/GetTransactions Get Transactions)
     * @param  string $date A date in the format yyyy-MM-dd
     * @return array        The transactions info or null on error
     */
    public function getTransactionsByDate($date)
    {
        $res = $this->getTransactions('date', $date);
        if ($res !== null) {
            return $res['Transactions'];
        }

        return null;
    }

    /**
     * Returns all transactions that were cleared on a specific date
     *
     * @link(https://github.com/VivaPayments/API/wiki/GetTransactions Get Transactions)
     * @param  string $date A date in the format yyyy-MM-dd
     * @return array       The transactions info or null on error
     */
    public function getClearedTransactions($date)
    {
        $res = $this->getTransactions('clearancedate', $date);
        if ($res !== null) {
            return $res['Transactions'];
        }

        return null;
    }

    /**
     * Cancel a card payment occurred within the same business day (till 22:00 GMT+2).
     *
     * @link https://github.com/VivaPayments/API/wiki/CancelTransaction
     * @param  string $txnId    A transaction ID
     * @return array            The transaction status ID or null on error. It should return
     */
    public function cancelTransaction($txnId)
    {
        $url = $this->createUrl("/api/transactions/{$txnId}");
        $response = null;
        try {
            $response = $this->getRestClient()
                ->delete($url);
        } catch (RequestException $e) {
            $this->error = $e->getMessage();
            return null;
        }

       $parsedResponse = $this->parseResponse($response);
       return $parsedResponse!==null ? $parsedResponse->StatusId : null;
    }

    /**
     * Confirms a transaction's validity
     *
     * @param  string $orderCode The order code from a previous order created by createOrder method
     * @param  string $txnId     A transaction id posted back by the provider
     * @return boolean
     */
    public function confirmTransaction($orderCode, $txnId)
    {
        $txn = $this->getTransactionInfo($txnId);

        if ($txn===null){
            $this->_errors[] = self::ERR_INVALID_TXNID;
            return false;
        }

        if ($txn['Order']['OrderCode'] != $orderCode) {
            $this->_errors[] = self::ERR_INVALID_ORDERID;
            return false;
        }

        $orderId = $txn['MerchantTrns'];
        $trans = $this->getPersistent()->findByAttributes(array('order_id'=>$orderId));

        if ($trans->txn_id !== null || $trans->status !== \CodeJunkii\VivaPayments\Client::TXN_STATUS_UNDERWAY) {
            $this->_errors[] = self::ERR_ALREADY_PROCESSED;
            return false;
        }

        if (round($trans->amount, 2) != round($txn['Amount'], 2)) {
            $this->_errors[] = self::ERR_AMOUNT_TAMPERED;
            return false;
        }

        return true;
    }


    /**
     * Make a partial or full refund of a successful payment that has already been cleared.
     *
     * @link https://github.com/VivaPayments/API/wiki/CancelTransaction
     * @param  string $txnId A transaction ID
     * @param  float $amount The amount that will be refunded in cents (amount in euros x 100). It should not exceed the of the original payment.
     *                       Note: If you want to create a payment for 100,37 €, you need to pass the value 10037
     * @return array        The transaction status ID or null on error
     */
    public function refund($txnId, $amount)
    {
        $params = array('amount'=>$this->centify($amount));
        if ($this->actionUser !== null)
            $params['actionUser']=$this->actionUser;

        $url = $this->createUrl("/api/transactions/{$txnId}", $params);
        $response = null;
        try {
            $response = $this->getRestClient()
                ->delete($url);
        } catch (RequestException $e) {
            $this->error = $e->getMessage();
            return null;
        }

       $parsedResponse = $this->parseResponse($response);
       return $parsedResponse!==null ? $parsedResponse->StatusId : null;
    }

    public function getWebhookAuthorizationToken()
    {
        $params = array();

        $url = $this->createUrl("/api/messages/config/token", $params);
        $response = null;
        try {
            $response = $this->getRestClient()->get($url);
        } catch (RequestException $e) {
            $this->error = $e->getMessage();
            return null;
        }
        $parsedResponse = $response->json();
        return $parsedResponse!==null ? $parsedResponse : null;

    }

    /**
     * Converts the amount into cents
     *
     * @param  float $amount An amount
     * @return integer       the amount changed into cents
     */
    protected function centify($amount)
    {
        return (int)round((float)$amount*100, 2);
    }

    /**
     * Gettransactions allows you to obtain:
     *  - Details for all transactions for a given Payment Order.
     *  - A list of all transactions that occurred on a given day.
     *
     * @param  string $paramName valid params are (txnid, ordercode, date, clearencedate)
     * @param  string $value     The param value
     * @return array  The transactions info or null on error
     */
    protected function getTransactions($paramName, $value)
    {
        $url = $paramName==='txnid'
            ? $this->createUrl("/api/transactions/{$value}")
            : $this->createUrl("/api/transactions", array($paramName=>$value));
        $response = null;
        try {
            $response = $this->getRestClient()
                ->get($url);
        } catch (RequestException $e) {
            $this->error = $e->getMessage();
            return null;
        }

        return $this->parseResponse($response);
    }

    /**
     * Create or just return a rest client instance
     *
     * @param boolean $refresh If set to TRUE it will discard any local cached object and will instantiate a new one.
     * @return \GuzzleHttp\Client
     */
    protected function getRestClient($refresh=false)
    {
        if ($this->_restClient===null||$refresh) {
            $this->_restClient = new \GuzzleHttp\Client(array(
                'defaults'=>array(
                    'auth'=>array($this->_merchantID, $this->_apiKey),
                    'debug'=>false
                )
            ));
        }

        return $this->_restClient;
    }

    /**
     * Creates a valid VivaPayments URL
     *
     * @param  string $path The path/route of the rest api endpoing (W/O host, W/O query params)
     * @param  array  $queryVars An array of key-value pairs with query string params
     * @return string An absolute url pointint at the api server
     */
    protected function createUrl($path, $queryVars=array())
    {
        $_url = constant('self::ENDPOINT_' . $this->_environment);
        $_url = rtrim($_url, '/') . '/' . trim($path, '/');
        $_q = http_build_query($queryVars, '&');
        return !empty($queryVars) ? "{$_url}?{$_q}" : $_url;
    }

    /**
     * Parses the response and returns just the API response
     *
     * @param  CodeJunkii\Util\RestClientResponse $response The rest response
     * @return array An array with the Viva response or null on error.
     */
    protected function parseResponse($response)
    {
        if ($response && $response->getStatusCode() == 200) {
            $resObj = $response->json();
            if ($resObj['ErrorCode']==0) {
                return $resObj;
            } else {
                $this->error = $resObj['ErrorText'];
            }
        } else {
            $this->error = $response->status;
        }

        return null;
    }
}
