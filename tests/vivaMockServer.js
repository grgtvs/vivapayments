/**
 * viva node.js test server to return queued responses to HTTP requests and
 * expose a RESTful API for enqueueing responses and retrieving the requests
 * that have been received.
 *
 * - Delete all requests that have been received:
 *      DELETE /viva-server/requests
 *      Host: 127.0.0.1:8125
 *
 *  - Enqueue responses
 *      PUT /viva-server/responses
 *      Host: 127.0.0.1:8125
 *
 *      [{ "statusCode": 200, "reasonPhrase": "OK", "headers": {}, "body": "" }]
 *
 *  - Get the received requests
 *      GET /viva-server/requests
 *      Host: 127.0.0.1:8125
 *
 *  - Shutdown the server
 *      DELETE /viva-server
 *      Host: 127.0.0.1:8125
 *
 * @package viva PHP <http://www.vivaphp.org>
 * @license See the LICENSE file that was distributed with this source code.
 */

var http = require("http");

/**
 * VivaPayments node.js server
 * @class
 */
var vivaMockServer = function(port, log) {

    this.port = port;
    this.log = log;
    this.responses = [];
    this.requests = [];
    var that = this;

    var controlRequest = function(request, req, res) {
        var url = require("url");
        _urlParts = url.parse(req.url, true);
        
        // Create order
        stdResponse = {
            OrderCode: 175936509216,
            ErrorCode: 0,
            ErrorText: "",
            TimeStamp: (new Date()).toJSON()
        };
        if (req.url.indexOf('/viva-server/api/orders') !== -1) {
            if (req.method === "POST") {
                switch(_urlParts.query.testCase) {
                   default:
                    strResponse = JSON.stringify(stdResponse);
                    res.writeHead(200, "OK", {"Content-Length": strResponse.length});
                    res.end(strResponse);
                }
            } else if (req.method === "DELETE") {
                orderCode = _urlParts.pathname.match(/^\/viva-server\/api\/orders\/(\d{12})$/g);
                switch(_urlParts.query.testCase) {
                    default:
                        stdResponse.OrderCode = orderCode;
                        strResponse = JSON.stringify(stdResponse);
                        res.writeHead(200, "OK", {"Content-Length": strResponse.length});
                        res.end(strResponse);
                }
            } else {
                // Response with an 400 error for invalid HTTP Verbs
                res.writeHead(400, "Bad Request", {"Content-Length": 5});
                res.end("error");
            }
        } else if (req.url === '/viva-server/perf') {
            t = (new Date()).getTime();
            res.writeHead(200, "OK", {"Content-Length": 20});
            res.end(t.toString());
        } else if (req.url === '/viva-server' && req.method === 'DELETE') {
            that.server.stop();
        } else {
            res.writeHead(404, "Not Found", {"Content-Length": 0});
            res.end();            
        }
    };

    var receivedRequest = function(request, req, res) {
        if (req.url.indexOf("/viva-server") === 0) {
            controlRequest(request, req, res);
        } else if (req.url.indexOf("/viva-server") == -1 && !that.responses.length) {
            res.writeHead(500);
            res.end("No responses in queue");
        } else {
            var response = that.responses.shift();
            res.writeHead(response.statusCode, response.reasonPhrase, response.headers);
            res.end(response.body);
            that.requests.push(request);
        }
    };

    this.start = function() {
        that.server = http.createServer(function(req, res) {
            var request = req.method + " " + req.url + " HTTP/" + req.httpVersion + "\r\n";
            for (var i in req.headers) {
                request += i + ": " + req.headers[i] + "\r\n";
            }
            request += "\r\n";

            // Receive each chunk of the request body
            req.addListener("data", function(chunk) {
                request += chunk;
            });

            // Called when the request completes
            req.addListener("end", function() {
                receivedRequest(request, req, res);
            });
        });
        that.server.listen(port, "127.0.0.1");

        if (this.log) {
            console.log("Server running at http://127.0.0.1:8125/");
        }
    };
};

// Get the port from the arguments
port = process.argv.length >= 3 ? process.argv[2] : 8125;
log = process.argv.length >= 4 ? process.argv[3] : false;

// Start the server
server = new vivaMockServer(port, log);
server.start();
