<?php
namespace CodeJunkii\VivaPayments\Tests;

use \CodeJunkii\VivaPayments\Client as VivaClient;
use \GuzzleHttp\Client;

/**
 * The Server class is used to control a scripted webserver using node.js that
 * will respond to HTTP requests with queued responses.
 *
 * Queued responses will be served to requests using a FIFO order.  All requests
 * received by the server are stored on the node.js server and can be retrieved
 * by calling {@see Server::received()}.
 *
 * Mock responses that don't require data to be transmitted over HTTP a great
 * for testing.  Mock response, however, cannot test the actual sending of an
 * HTTP request using cURL.  This test server allows the simulation of any
 * number of HTTP request response transactions to test the actual sending of
 * requests over the wire without having to leave an internal network.
 */
class VivaServerMock
{
    const REQUEST_DELIMITER = "\n----[request]\n";

    /** @var Client */
    private static $client;

    public static $started;
    public static $url = 'http://127.0.0.1:8125/';
    public static $port = 8125;

    /**
     * Get all of the received requests
     *
     * @param bool $hydrate Set to TRUE to turn the messages into
     *      actual {@see RequestInterface} objects.  If $hydrate is FALSE,
     *      requests will be returned as strings.
     *
     * @return array
     * @throws \RuntimeException
     */
    public static function received($hydrate = false)
    {
        if (!self::$started) {
            return [];
        }

        $response = self::getClient()->get('guzzle-server/requests');
        $data = array_filter(explode(self::REQUEST_DELIMITER, (string) $response->getBody()));
        if ($hydrate) {
            $factory = new MessageFactory();
            $data = array_map(function($message) use ($factory) {
                return $factory->fromMessage($message);
            }, $data);
        }

        return $data;
    }

    /**
     * Stop running the node.js server
     */
    public static function stop()
    {
        if (self::$started) {
            self::getClient()->delete('viva-server');
        }

        self::$started = false;
    }

    public static function wait($maxTries = 5)
    {
        $tries = 0;
        while (!self::isListening() && ++$tries < $maxTries) {
            usleep(100000);
        }

        if (!self::isListening()) {
            throw new \RuntimeException('Unable to contact node.js server');
        }
    }

    public static function start()
    {
        if (self::$started){
            return;
        }

        if (!self::isListening()) {
            exec('nodejs ' . __DIR__ . \DIRECTORY_SEPARATOR . 'vivaMockServer.js '
                . self::$port . ' >> /tmp/server.log 2>&1 &');
            // self::wait();
        }

        self::$started = true;
    }

    private static function isListening()
    {
        try {
            self::getClient()->get('/viva-server/perf', [
                'connect_timeout' => 5,
                'timeout'         => 5
            ]);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private static function getClient()
    {
        if (!self::$client) {
            self::$client = new Client(array('base_url'=>self::$url));
        }

        return self::$client;
    }
}
