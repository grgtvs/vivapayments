<?php
namespace CodeJunkii\VivaPayments\Tests;

use CodeJunkii\VivaPayments\Client;

class VivaClientTest extends \PHPUnit_Framework_TestCase
{ 
    public 
        $merchanID  = "MerchantID",
        $appKey     = "AppKey",
        $client = null;

    public function setUp()
    {
        $this->client = new Client(Client::ENV_TEST, $this->merchanID, $this->appKey);
    }

    public function testClientInit()
    {
        $this->assertInstanceOf('CodeJunkii\VivaPayments\Client',$this->client);
        
        $method = $this->getMethodToTest($this->client, "getRestClient");
        // Test the rest client set up
        $restClient = $method->invoke($this->client);
        $restClient2 = $method->invoke($this->client);
        $restClient3 = $method->invoke($this->client, true);

        $this->assertInstanceOf('GuzzleHttp\Client', $restClient);
        $this->assertEquals(spl_object_hash($restClient), spl_object_hash($restClient2), "");
        $this->assertNotEquals(spl_object_hash($restClient), spl_object_hash($restClient3));

        // Test that correct auth credentials has been passed into the rest client
        $auth = $restClient->getDefaultOption('auth');
        $this->assertEquals($auth[0], $this->merchanID, "The merchant ID is invalid");
        $this->assertEquals($auth[1], $this->appKey, "The app key is invalid");
    }

    public function testCreateApiUrls()
    {
        $method = $this->getMethodToTest($this->client, "createUrl");
        $url = $method->invoke($this->client, "/foo/bar/");
        $urlWithParams = $method->invoke($this->client, "/foo/bar/", array('test'=>'test', 'test2'=>'test2'));
        $urlWithArray= $method->invoke($this->client, "/foo/bar/", array('test'=>'test', 'test2'=>'test2', 'array'=>array(1,2,3)));
        $urlWithBoolean= $method->invoke($this->client, "/foo/bar/", array('test'=>'test', 'test2'=>'test2', 'boolTrue'=>true, 'boolFalse'=>false));
        // Test the url creation using various types
        $this->assertEquals(Client::ENDPOINT_TEST."/foo/bar", $url);
        $this->assertEquals(Client::ENDPOINT_TEST."/foo/bar?test=test&test2=test2", urldecode($urlWithParams));
        $this->assertEquals(Client::ENDPOINT_TEST."/foo/bar?test=test&test2=test2&array[0]=1&array[1]=2&array[2]=3", urldecode($urlWithArray));
        $this->assertEquals(Client::ENDPOINT_TEST."/foo/bar?test=test&test2=test2&boolTrue=1&boolFalse=0", urldecode($urlWithBoolean));
    
        // Test the url creation in other environments too.
        $clientSandbox      = new Client(Client::ENV_SANDBOX, $this->merchanID, $this->appKey);
        $clientProduction   = new Client(Client::ENV_PRODUCTION, $this->merchanID, $this->appKey);

        $sandboxUrl     = $method->invoke($clientSandbox,       "/foo/bar/");        
        $productionUrl  = $method->invoke($clientProduction,    "/foo/bar/");        

        $this->assertEquals(Client::ENDPOINT_SANDBOX    ."/foo/bar",    $sandboxUrl);
        $this->assertEquals(Client::ENDPOINT_PRODUCTION ."/foo/bar", $productionUrl);
    }

    public function testCreateOrder()
    {
        $amount = 100;
        $params = array();
        $order = $this->client->createOrder($amount, $params);
	
	$this->assertNotEmpty($order);
    }


    /**
     * Returns ReflactionMethod ready to be invoked. Useful for testing
     * protected or private methods
     * 
     * @param  mixed  $obj        An object
     * @param  string $methodName A method name
     * @return \ReflectionMethod    The method as a reflection object
     */
    private function getMethodToTest($obj, $methodName)
    {
        $refl = new \ReflectionObject($obj);
        $method = $refl->getMethod($methodName);
        $method->setAccessible(true);
        return $method;
    }
}
