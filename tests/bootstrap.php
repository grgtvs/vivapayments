<?php

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/VivaServerMock.php';

use CodeJunkii\VivaPayments\Tests\VivaServerMock as Server;

Server::start();

register_shutdown_function(function () {
    if (Server::$started) {
        Server::stop();
    }
});
